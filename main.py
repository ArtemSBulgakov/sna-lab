from fastapi import FastAPI
from date_formatter import get_current_date

app = FastAPI()


@app.get("/")
def read_root():
    return {"date": get_current_date()}
