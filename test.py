from datetime import datetime


def test_current_date_formatting():
    from date_formatter import get_current_date

    assert get_current_date() == datetime.now().strftime("%Y-%m-%d")
